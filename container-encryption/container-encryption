#!/bin/bash

CONTAINER_FILE=
CONTAINER_NAME=
CONTAINER_SIZE=
USER=christian
MOUNT_DIRECTORY=/media/$USER
FREE_LOOPDEV=`losetup -f`
USED_LOOPDEV=`losetup -a | grep "$CONTAINER_FILE" | sed "s/: .*//"`
CREATE=
MOUNT=
UMOUNT=

if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

function print_help {
  echo -e "Create, mount and umount an encrypted container"
  echo -e "Usage:"
  echo -e "\t container-encryption create|mount|umount [options]"
  echo -e "Options:"
  echo -e "\t -f, --container-file <file path> \t name and path of the container file"
  echo -e "\t -n, --container-name <name>      \t container name"
  echo -e "\t -s, --container-size <size>      \t container size given in MB"
  echo -e "\t -u, --user <user>                \t user name"
  echo -e "\t -m, --mount-directory <directory>\t mount directory in which the container folder will be created and mounted"
  echo -e "\t -h, --help                       \t display this help"
}

while(( "$#" )); do
  case "$1" in
    create)
      CREATE=true
      shift
      ;;
    mount)
      MOUNT=true
      shift
      ;;
    umount)
      UMOUNT=true
      shift
      ;;
    -f|--container-file)
      CONTAINER_FILE=$2
      shift 2
      ;;
    -n|--container-name)
      CONTAINER_NAME=$2
      shift 2
      ;;
    -s|--container-size)
      CONTAINER_SIZE=$2
      shift 2
      ;;
    -m|--mount-directory)
      MOUNT_DIRECTORY=$2
      shift 2
      ;;
    -u|--user)
      USER=$2
      shift 2
      ;;
    -h|--help)
      print_help
      exit
      ;;
    *)
      print_help
      exit
      ;;
  esac
done


function create_container {
  dd if=/dev/urandom of=$CONTAINER_FILE bs=1M count=$CONTAINER_SIZE
  losetup $FREE_LOOPDEV $CONTAINER_FILE
  cryptsetup -c aes-xts-plain64 -s 512 -h sha512 -y luksFormat $FREE_LOOPDEV
  cryptsetup luksOpen $FREE_LOOPDEV $CONTAINER_NAME
  mkfs.ext4 /dev/mapper/$CONTAINER_NAME
  mkdir -p $MOUNT_DIRECTORY/$CONTAINER_NAME
  mount -t ext4 /dev/mapper/$CONTAINER_NAME $MOUNT_DIRECTORY/$CONTAINER_NAME
  chown -R $USER:$USER $MOUNT_DIRECTORY/$CONTAINER_NAME
  chown -R $USER:$USER $CONTAINER_FILE
  umount $MOUNT_DIRECTORY/$CONTAINER_NAME
  cryptsetup luksClose $CONTAINER_NAME
  losetup -d $FREE_LOOPDEV
}

function mount_container {
  if [ "`losetup -a | grep -c "$CONTAINER_FILE"`" != "0" ]; then
    echo "already mounted"
    exit
  fi

  mkdir -p $MOUNT_DIRECTORY/$CONTAINER_NAME
  losetup $FREE_LOOPDEV $CONTAINER_FILE
  cryptsetup luksOpen $FREE_LOOPDEV $CONTAINER_NAME
  mount -t ext4 /dev/mapper/$CONTAINER_NAME $MOUNT_DIRECTORY/$CONTAINER_NAME
}

function umount_container {
  if [ "`losetup -a | grep -c "$CONTAINER_FILE"`" != "1" ]; then
    echo "not mounted"
    exit
  fi

  umount $MOUNT_DIRECTORY/$CONTAINER_NAME
  cryptsetup luksClose $CONTAINER_NAME
  losetup -d $USED_LOOPDEV
  rmdir $MOUNT_DIRECTORY/$CONTAINER_NAME
}


if [[ $CREATE ]]; then
  if [[ $CONTAINER_FILE && $CONTAINER_NAME && $CONTAINER_SIZE && $USER ]]; then
    create_container
  else
    print_help
  fi
elif [[ $MOUNT ]]; then
  if [[ $CONTAINER_FILE && $CONTAINER_NAME ]]; then
    mount_container
  else
    print_help
  fi
elif [[ $UMOUNT ]]; then
  if [[ $CONTAINER_FILE && $CONTAINER_NAME ]]; then
    umount_container
  else
    print_help
  fi
fi
