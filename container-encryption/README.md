# Container Encryption

## Install script

```bash
sudo ./install.sh
```

## Usage
```bash
Create, mount and umount an encrypted container
Usage:
         container-encryption create|mount|umount [options]
Options:
         -f, --container-file <file path>        name and path of the container file
         -n, --container-name <name>             container name
         -s, --container-size <size>             container size given in MB
         -u, --user <user>                       user name
         -m, --mount-directory <directory>       mount directory in which the container folder will be created and mounted
         -h, --help                              display this help
```

## Create new container (100 MB)

```bash
sudo container-encryption create -f /tmp/container -n test -u user -s 100
```

## Mount container

```bash
sudo container-encryption mount -f /tmp/container -n test -u user
```

## Umount container

```bash
sudo container-encryption umount -f /tmp/container -n test -u user
```
