lvim.leader = "\\"

lvim.plugins = {
  { "tanvirtin/monokai.nvim" },
  { "fatih/molokai" },
  {
    'alexghergh/nvim-tmux-navigation',
    config = function()
      require 'nvim-tmux-navigation'.setup {
        disable_when_zoomed = true,     -- defaults to false
        keybindings = {
          left = "<C-h>",
          down = "<C-j>",
          up = "<C-k>",
          right = "<C-l>",
          last_active = "<C-\\>",
          next = "<C-Space>",
        }
      }
    end
  }
}

lvim.colorscheme = "monokai"

lvim.format_on_save.enabled = true

lvim.keys.normal_mode["<F4>"] = ":bp<CR>"
lvim.keys.normal_mode["<F5>"] = ":bn<CR>"

local formatters = require "lvim.lsp.null-ls.formatters"
formatters.setup {
    command = "prettier",
    filetypes = { "typescript", "typescriptreact", "javascript", "javascriptreact" },
}

local linters = require "lvim.lsp.null-ls.linters"
linters.setup {
  { command = "eslint", filetypes = { "javascript", "typescript", "typescriptreact", "javascriptreact" } },
}

lvim.format_on_save = true

local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>f', builtin.find_files, {})
vim.keymap.set('n', '<leader>r', builtin.live_grep, {})

