#!/bin/bash

. ${0%/*}/colours.sh

INTERFACE=`ip -4 route ls | grep default | grep -Po '(?<=dev )(\S+)'`

SENT_BYTES_0=`cat /sys/class/net/$INTERFACE/statistics/tx_bytes`
sleep 0.25
SENT_BYTES_1=`cat /sys/class/net/$INTERFACE/statistics/tx_bytes`

SENT=`expr $(expr $SENT_BYTES_1 - $SENT_BYTES_0) / 256 `
print_white_string "🡅 ${SENT}KB"
