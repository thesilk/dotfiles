#!/bin/bash

. ${0%/*}/colours.sh

INTERFACE=`ip -4 route ls | grep default | grep -Po '(?<=dev )(\S+)'`

RECEIVED_BYTES_0=`cat /sys/class/net/$INTERFACE/statistics/rx_bytes`
sleep 0.25
RECEIVED_BYTES_1=`cat /sys/class/net/$INTERFACE/statistics/rx_bytes`

RECEIVED=`expr $(expr $RECEIVED_BYTES_1 - $RECEIVED_BYTES_0) / 256 `
print_white_string "🡇 ${RECEIVED}KB"
