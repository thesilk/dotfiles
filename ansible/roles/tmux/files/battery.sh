#!/bin/bash

. ${0%/*}/colours.sh

main() {
    charge_now="$(cat /sys/class/power_supply/BAT0/charge_now)"
    charge_full="$(cat /sys/class/power_supply/BAT0/charge_full)"

    charge_percentage="$(awk '{printf("%i%",$1/$2*100)}' <<<" ${charge_now} ${charge_full} ")"

    if [[ "${charge_percentage}" -lt 10 ]]; then
        print_red_string "🔋${charge_percentage}"
    elif [[ "${charge_percentage}" -lt 30 ]]; then
        print_yellow_string "🔋${charge_percentage}"
    else
        print_green_string "🔋${charge_percentage}"
    fi

}

main "$@"
