#!/bin/bash
SESSION=work
tmux="tmux"

if [[ -n "$1" ]]; then
    DEFAULT_DIR=$1
else
    DEFAULT_DIR=$HOME
fi

# if the session is already running, just attach to it.
$tmux has-session -t $SESSION
if [ $? -eq 0 ]; then
       echo "Session $SESSION already exists. Attaching."
       sleep 1
       $tmux attach -t $SESSION
       exit 0;
fi

# create a new session, named $SESSION, and detach from it
$tmux new-session -d -s $SESSION -n "IDE" -c $DEFAULT_DIR
$tmux new-window    -t $SESSION:0 
$tmux split-window  -v -p 10 -t $SESSION:0 -c $DEFAULT_DIR
$tmux new-window    -t $SESSION:1 -n "Term" -c $HOME
$tmux select-window -t $SESSION:0
$tmux attach -t $SESSION
