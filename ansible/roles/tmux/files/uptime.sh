#!/bin/zsh

main() {
    uptime="$(awk '{ print $1 }' < /proc/uptime)"

    min=${$(expr $(($uptime/60)))%.*}
    hours=$(expr $(($min/60)))
    min=$(expr $((min-hours*60)))

    if [[ "${min}" -lt 10 ]]; then
        echo "${hours}:0${min}"
    else
        echo "${hours}:${min}"
    fi
}

main "$@"
