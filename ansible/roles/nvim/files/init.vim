syntax enable
set tabstop=4
set expandtab
set number
filetype indent on
set autoindent
set autowrite
set list listchars=tab:❘-,trail:·,extends:»,precedes:«,nbsp:×

" Search down into subfolders
" Privides tab-completion for all file-related tasks
set path+=**

" Display all matching files when we tab complete
set wildmenu

autocmd Filetype css setlocal tabstop=2 shiftwidth=2
autocmd Filetype scss setlocal tabstop=2 shiftwidth=2
autocmd Filetype vue setlocal tabstop=2 shiftwidth=2
autocmd Filetype yaml setlocal tabstop=2 shiftwidth=2
autocmd Filetype conf setlocal tabstop=4 shiftwidth=4 softtabstop=4 noexpandtab
autocmd Filetype go setlocal tabstop=4 shiftwidth=4 softtabstop=4 noexpandtab
autocmd Filetype html setlocal tabstop=2 shiftwidth=2
autocmd Filetype make setlocal tabstop=8
autocmd Filetype cucumber setlocal tabstop=2 softtabstop=2 shiftwidth=2 expandtab
autocmd Filetype typescript setlocal tabstop=2 softtabstop=2 shiftwidth=2 expandtab
autocmd Filetype javascript setlocal tabstop=2 softtabstop=2 shiftwidth=2 expandtab
autocmd Filetype json setlocal tabstop=2 softtabstop=2 shiftwidth=2 expandtab

set nocompatible
filetype off
call plug#begin()
Plug 'neoclide/coc.nvim', {'branch': 'release', 'do': 'yarn install --frozen-lockfile'}
Plug 'w0rp/ale'
Plug 'fatih/vim-go'
Plug 'AndrewRadev/splitjoin.vim'
Plug 'ervandew/supertab'
Plug 'SirVer/ultisnips'
Plug 'fatih/molokai'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'mileszs/ack.vim'
Plug 'scrooloose/nerdcommenter'
Plug 'jiangmiao/auto-pairs'
Plug 'dhruvasagar/vim-table-mode'
Plug 'Yggdroot/indentLine'
Plug 'mattn/emmet-vim'
Plug 'tpope/vim-fugitive'
Plug 'gregsexton/gitv'
Plug 'airblade/vim-gitgutter'
Plug 'leafgarland/typescript-vim'
Plug 'Shougo/vimproc.vim'
Plug 'Quramy/tsuquyomi'
Plug 'tpope/vim-cucumber'
Plug 'vim-airline/vim-airline'
Plug 'jlanzarotta/bufexplorer'
Plug 'christoomey/vim-tmux-navigator'
Plug 'docunext/closetag.vim', { 'for': ['html', 'xml'] }
Plug 'pangloss/vim-javascript', {'for': 'javascript'}
Plug 'mxw/vim-jsx', {'for': 'javascript'}
Plug 'cakebaker/scss-syntax.vim'
Plug 'posva/vim-vue'
Plug 'ekalinin/Dockerfile.vim', {'for': 'Dockerfile'}
Plug 'fatih/vim-nginx' , {'for': 'nginx'}
call plug#end()
filetype plugin indent on

autocmd BufRead,BufNewFile *.vue setlocal filetype=vue
let g:NERDSpaceDelims = 1

nnoremap <silent> <leader>f :Files<cr>
nnoremap <silent> <leader>r :Rg<cr>

let g:rehash256 = 1
let g:molokai_original = 1
colorscheme molokai

let g:go_fmt_command = "goimports"
let g:go_list_type = "quickfix"
let g:go_def_mode="gopls"
let g:go_info_mode="gopls"
let g:go_def_mapping_enabled = 0

let g:go_metalinter_enabled = ['vet', 'golint', 'errcheck']
let g:go_metalinter_autosave = 1
let g:go_metalinter_deadline = "5s"

let g:go_auto_type_info = 1
set updatetime=100

let g:go_auto_sameids = 1

let g:go_highlight_fields = 1
let g:go_highlight_types = 1
let g:go_highlight_functions = 0
let g:go_highlight_function_calls = 1
let g:go_highlight_operators = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_build_constraints = 1

let g:table_mode_corner='|'

" better key bindings for UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger = "<tab>"
let g:UltiSnipsJumpForwardTrigger = "<tab>"
let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"

let g:indentLine_setColors = 239
let g:indentLine_char = 'c'

let g:airline#extensions#tabline#enabled = 1

 ""Buffers - next/previous: F5, F4.
nnoremap <silent> <F4> :bp<CR>
nnoremap <silent> <F5> :bn<CR>
"let g:ctrlp_cmd = 'CtrlPBuffer'


map <C-n> :cnext<CR>
map <C-m> :cprevious<CR>
nnoremap <leader>a :cclose<CR>
nnoremap <leader>t :TslintFix<CR>

autocmd FileType go nmap <Leader>i <Plug>(go-info)
autocmd FileType go nmap <C-d>d <Plug>(go-decls)
autocmd FileType go nmap <C-d>D <Plug>(go-decls-dir)

map <C-e> :Explore!<CR>

let g:user_emmet_install_global = 0
autocmd FileType html,css EmmetInstall
let g:user_emmet_leader_key='<C-Z>'

let g:typescript_indent_disable = 1
autocmd FileType typescript setlocal indentkeys+=0.
let g:typescript_compiler_binary = 'tsc'
let g:typescript_compiler_options = ''
let g:typescript_ignore_typescriptdoc = 1


hi Comment ctermfg=LightBlue

hi default link CocErrorSign Error
hi default link CocWarningSign Error
hi default link CocInfoSign Exception
hi default link CocHintSign Exception
"" Use <c-n> for trigger completion.
inoremap <silent><expr> <c-n> coc#refresh()
"" Use <cr> for confirm completion, `<C-g>u` means break undo chain at current position.
"" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
"" Use `[c` and `]c` for navigate diagnostics
nmap <silent> [c <Plug>(coc-diagnostic-prev)
nmap <silent> ]c <Plug>(coc-diagnostic-next)
"" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gt <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
" Use K for show documentation in preview window
nnoremap <silent> <s-k> :call <SID>show_documentation()<CR>
function! s:show_documentation()
  if &filetype == 'vim'
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

let languageservers = {}
let languageservers['golang'] = {
    \ 'command': 'gopls',
    \ 'rootPatterns': ['go.mod', '.vim/', '.git/', '.hg/', 'vendor/'],
    \ 'filetypes': ['go']
    \ }
let languageservers['bash'] = {
    \ 'command': 'bash-language-server',
    \ 'args': ['start'],
    \ 'filetypes': ['sh'],
    \ 'ignoredRootPaths': ['~']
    \ }
" https://github.com/neoclide/coc.nvim#extensions
let g:coc_global_extensions = [
    \ 'coc-pairs',
    \ 'coc-prettier',
    \ 'coc-lists',
    \ 'coc-dictionary',
    \ 'coc-word',
    \ 'coc-emmet',
    \ 'coc-highlight',
    \ 'coc-angular',
    \ 'coc-eslint',
    \ 'coc-html',
    \ 'coc-css',
    \ 'coc-tsserver',
    \ 'coc-tslint-plugin',
    \ 'coc-stylelint',
    \ 'coc-gocode',
    \ 'coc-json',
    \ 'coc-yaml',
    \ 'coc-vetur',
    \ 'coc-snippets',
    \ 'coc-python',
    \]
let g:coc_user_config = {
    \ 'suggest.detailMaxLength': 111,
    \ 'suggest.maxCompleteItemCount': 48,
    \ 'diagnostic.enable': v:true,
    \ 'diagnostic.enableMessage': 'always',
    \ 'diagnostic.displayByAle': v:false,
    \ 'diagnostic.errorSign': '✗',
    \ 'diagnostic.warningSign': '∙',
    \ 'diagnostic.infoSign': '∙',
    \ 'diagnostic.hintSign': '∙',
    \ 'languageserver': languageservers,
    \ }

" coc-pairs
autocmd FileType vim let b:coc_pairs_disabled = ['"']

hi default link ALEErrorSign Error
hi default link ALEWarningSign Exception
let g:ale_completion_enabled = 0
let g:ale_history_log_output = 0  " Save memory.
let g:ale_history_enabled = 0
let g:ale_sign_error = '✗'
let g:ale_sign_warning = '∙'
let g:airline#extensions#ale#enabled = 1
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%severity%] [%linter%] %(code): %%s'
augroup CloseLoclistWindowGroup
  autocmd!
  autocmd QuitPre * if empty(&buftype) | lclose | endif
augroup END
let g:ale_list_window_size = 5
let g:ale_fix_on_save = 1
let g:ale_fixers = {
      \'*': ['remove_trailing_lines', 'trim_whitespace'],
      \'go': ['goimports'],
      \}
let g:ale_lint_delay = 111
let g:ale_linter_aliases = {'vue': ['vue', 'javascript']}
let g:ale_linters = {
\        'vim': ['vint'],
\        'sh': ['shellcheck'],
\        'typescript': ['tsserver', 'tslint'],
\        'vue': ['eslint', 'vls'],
\        'html': ['alex'],
\        }
let g:ale_linters_explicit = 1
let g:ale_vim_vint_show_style_issues = 0
let g:ale_sh_shellcheck_options = '-x'
let g:ale_go_langserver_executable = 'gopls'

" HTML/CSS/Javascript
let g:closetag_html_style = 1

command! -nargs=0 Prettier :CocCommand prettier.formatFile

let g:prettier#autoformat = 0
autocmd BufWritePre *.js,*.jsx,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.vue,*.yaml,*.html Prettier

function ToggleCopyMode()
                 set list!
                 set number!
endfunction
