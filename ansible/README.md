# Install Default Settings

In this folder there is a playbook `default-setup.yml` which installs the following roles:

- [oh-my-zsh](https://github.com/ohmyzsh/ohmyzsh)
- [tmux](https://github.com/tmux/tmux)
- [nvm](https://github.com/nvm-sh/nvm)
- [neovim](https://github.com/neovim/neovim)
- custom keyboard setting

## Prerequisite

To use the ansible playbook you have to install these following tools:

```bash
$ sudo apt update
$ sudo apt install python3-pip python3-dev git
$ pip3 install ansible --user
```

## Run the playbook

### Remote

You have to add the remote host in the `inventory.ini`.

```bash
$ ANSIBLE_SSH_ARGS='-o IdentitiesOnly=yes' ansible-playbook -i ./inventory.ini --ask-pass --user thesilk --ask-become-pass default-setup.yml
```

### Localhost

It's also possible to use the playbook locally with the following command:

```bash
$ ansible-playbook  --connection=local --inventory 127.0.0.1, --limit 127.0.0.1 --ask-become-pass --user thesilk default-setup.yml
```

The comma behind `--inventory 127.0.0.1` is important. Without it won't work.
